﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stuff_File_Browser
{
    public class StuffFileReader
    {
        UsableFileStream stuffFile;
        ListView browser;
        ImageList imageList;

        StuffHeader fileHeader;
        Node fileTree;
        long dataBaseAddress;

        public StuffFileReader(ListView tree)
        {
            browser = tree;
            imageList = new ImageList();
            dataBaseAddress = 0xBADBAD;
            tree.MouseDoubleClick += tree_MouseDoubleClick;
            //tree.LargeImageList = imageList;

            //imageList.Images.Add("folder", null);
        }

        void tree_ItemDrag(object sender, ItemDragEventArgs e)
        {
            (sender as ListView).DoDragDrop(e.Item, DragDropEffects.Copy);
        }

        void tree_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var item = browser.SelectedItems.Count > 0 ? browser.SelectedItems[0] : null;
            if (item == null)
                return;

            if (item.Tag is DirectoryNode)
                UpdateListView(item.Tag as DirectoryNode);
            else
            {
                var node = item.Tag as FileNode;
                stuffFile.Position = node.FileOffset;
                string temp = Path.GetTempPath();
                File.WriteAllBytes(Path.Combine(temp, node.Name), stuffFile.Read(0, node.FileHeader.FileSize));
                System.Diagnostics.Process.Start(Path.Combine(temp, node.Name));
            }
        }

        public void LoadFile(string path)
        {
            stuffFile = new UsableFileStream(File.OpenRead(path));
            fileHeader = new StuffHeader();
            fileHeader.FileCount = BitConverter.ToInt32(stuffFile.Read(0, 4), 0);
            fileHeader.FileHeaders = new FileHeader[fileHeader.FileCount];
            for (int i = 0; i < fileHeader.FileCount; i++)
            {
                FileHeader fileHdr = new FileHeader { FileSize = BitConverter.ToInt32(stuffFile.Read(0, 4), 0), FileNameLength = BitConverter.ToInt32(stuffFile.Read(0, 4), 0) };
                fileHdr.FileName = Encoding.ASCII.GetString(stuffFile.Read(0, fileHdr.FileNameLength));
                fileHeader.FileHeaders[i] = fileHdr;
                stuffFile._stream.Position += 1;
            }
            dataBaseAddress = stuffFile.Position;
            fileTree = ConstructFileTree();
            UpdateListView(fileTree);
        }

        Node ConstructFileTree()
        {
            DirectoryNode Root = new DirectoryNode("", null);
            long relativeOffset = 0x0;
            foreach (FileHeader file in fileHeader.FileHeaders)
            {
                string[] PathAndFile = file.FileName.Split('\\');
                FileNode fileNode = new FileNode(PathAndFile[PathAndFile.Length - 1], relativeOffset + dataBaseAddress, file, null);
                Root.AddPath(PathAndFile, fileNode);
                relativeOffset += file.FileSize;
            }
            return Root;
        }

        void UpdateListView(Node newNode)
        {
            List<ListViewItem> items = new List<ListViewItem>();
            if (newNode.Parent != null)
                items.Add(new ListViewItem { Name = "..", Text = "..", Tag = newNode.Parent, ImageIndex = 1 });
            foreach (Node child in newNode.GetChildren())
            {
                items.Add(new ListViewItem
                {
                    Name = child.Name,
                    Text = child.Name,
                    Tag = child,
                    ImageIndex = child is DirectoryNode ? 1 : 0 //"folder" : imageList.Images.ContainsKey(Path.GetExtension(child.Name)) ? Path.GetExtension(child.Name) : "blank"
                });
            }
            browser.Invoke(new Action(() => { browser.Items.Clear(); browser.Items.AddRange(items.ToArray()); }));
        }

        public class StuffHeader
        {
            public int FileCount;
            public FileHeader[] FileHeaders;
        }

        public class FileHeader
        {
            public int FileSize;
            public int FileNameLength;
            public string FileName;
        }
    }

    public class UsableFileStream
    {
        public FileStream _stream;
        public UsableFileStream(FileStream stream)
        { _stream = stream; }

        public long Position
        {
            get { return _stream.Position; }
            set { _stream.Position = value; }
        }

        public byte[] Read(int offset, int count)
        {
            byte[] temp = new byte[count];
            _stream.Read(temp, offset, count);
            return temp;
        }
    }

    public class Node
    {
        public DirectoryNode Parent;
        public string Name;

        public List<Node> Children;

        public Node(string name, DirectoryNode parent)
        {
            Name = name;
            Parent = parent;
            Children = new List<Node>();
        }

        public T[] GetChildrenOfType<T>()
        {
            return Children.OfType<T>().ToArray();
        }

        public Node[] GetChildren()
        {
            return Children.ToArray();
        }

        public Node GetChild(int i)
        {
            return Children[i];
        }

        public Node GetChild(string name)
        {
            return Children.Find(node => node.Name.Equals(name));
        }

        public void AddChild(Node node)
        {
            Children.Add(node);
        }

        public void AddPath(string[] path, Node node)
        {
            if (!(this is DirectoryNode))
                throw new InvalidOperationException("Cannot add children to a FileNode");
            DirectoryNode prevNode = (DirectoryNode)this;
            for (int i = 0; i < path.Length - 1; i++)
            {
                string dir = path[i];
                Node nextNode = prevNode.GetChild(dir);
                if (nextNode != null && nextNode is DirectoryNode)
                {
                    prevNode = (DirectoryNode)nextNode;
                }
                else if (nextNode == null)
                {
                    nextNode = new DirectoryNode(dir, prevNode);
                    prevNode.AddChild(nextNode);
                    prevNode = (DirectoryNode)nextNode;
                }
                else
                {
                    throw new InvalidOperationException("Node already exists as FileNode");
                }
            }
            if (prevNode != null && prevNode is DirectoryNode)
            {
                node.Parent = prevNode;
                prevNode.AddChild(node);
            }
        }
    }

    public class DirectoryNode : Node
    {
        public DirectoryNode(string name, DirectoryNode parent)
            : base(name, parent)
        { }


    }

    public class FileNode : Node
    {
        public FileNode(string name, long offset, StuffFileReader.FileHeader header, DirectoryNode parent)
            : base(name, parent)
        {
            FileOffset = offset;
            FileHeader = header;
        }

        public long FileOffset;
        public StuffFileReader.FileHeader FileHeader;
    }
}
