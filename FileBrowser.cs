﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stuff_File_Browser
{
    public partial class FileBrowser : Form
    {
        public FileBrowser()
        {
            InitializeComponent();
        }

        StuffFileReader stuffFile;

        private void listView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (files.Length > 1)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (files.Any(_ =>
                {
                    var ext = Path.GetExtension(_).ToLower();
                    return ext.Equals(".stuff");
                }))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void listView1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (files.Length > 1)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (Path.GetExtension(files[0]).ToLower().Equals(".stuff"))
                {
                    e.Effect = DragDropEffects.Copy;
                    stuffFile = new StuffFileReader(this.listView1);
                    stuffFile.LoadFile(files[0]);
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
    }
}
